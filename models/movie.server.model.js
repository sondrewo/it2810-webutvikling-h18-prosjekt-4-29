/**
 * What: MongoDb Model for Movies
 *
 * How: Create new Mongoose Schema that matches object structure in DB.
 *
 * Returns: Nothing
 */

var mongoose = require("mongoose");

var movieSchema = mongoose.Schema({
  _id: Number,
  original_title: String,
  year: String,
  director: [{ name: String, id: Number }],
  genres: [String],
  feedbackList: [
    {
      name: String,
      rating: Number,
      review: String
    }
  ]
});

var Movie = mongoose.model("movies", movieSchema);
module.exports = Movie;
