import mongoose from "mongoose";
import Movie from "../models/movie.server.model";

/**
 * What: Method for DB query based on request paramters.
 *
 * How: Recieves parameters via REST; filter, sort, text.
 * Constructs mongoose query based on those paramters.
 *
 * Returns: Movies that matches query
 */

export const getMovie = (req, res) => {
  console.log(req.params);
  let request = {};

  if (req.params.text !== "None") {
    let regex = new RegExp(req.params.text, "i");
    request["original_title"] = regex;
  }

  if (req.params.year !== "None") {
    request["year"] = req.params.year;
  }

  if (req.params.genre !== "None") {
    let regex = new RegExp(req.params.genre, "i");
    request["genres"] = regex;
  }

  Movie.find(request)
    .limit(1000)
    .exec((err, movie) => {
      if (err) {
        return res.json({ success: false, message: "Some Error" });
      }
      if (movie.length) {
        return res.json({
          success: true,
          message: "Movie fetched successfully",
          movie
        });
      } else {
        return res.json({
          success: false,
          message: "failed to fetch movie"
        });
      }
    });
};
