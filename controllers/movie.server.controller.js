import mongoose from "mongoose";
import Movie from "../models/movie.server.model";

/**
 * What: Basic CRUD operations for data-handling.
 *
 * How: Uses MongoDb Model to create,read, update & delete objects in Schema.
 *
 */

// Returns: movies that matches query
export const getMovies = (req, res) => {
  Movie.find().exec((err, movies) => {
    if (err) {
      return res.json({ success: false, message: "Some Error" });
    }
    return res.json({
      success: true,
      message: "movies fetched successfully",
      movies
    });
  });
};

// Returns: added movie
export const addMovie = (req, res) => {
  const newMovie = new Movie(req.body);
  newMovie.save((err, movies) => {
    if (err) {
      return res.json({ success: false, message: "Some Error" });
    }
    return res.json({
      success: true,
      message: "Movie added successfully",
      movies
    });
  });
};

// Returns: updated movies
export const updateMovie = (req, res) => {
  Movie.findOneAndUpdate(
    { _id: req.body._id },
    req.body,
    { new: true },
    (err, movie) => {
      if (err) {
        return res.json({ success: false, message: "Some Error", error: err });
      }
      console.log(movie);
      return res.json({
        success: true,
        message: "Updated successfully",
        movie
      });
    }
  );
};

// Returns: movie that matches ID
export const getMovie = (req, res) => {
  Movie.find({ _id: req.params.id }).exec((err, movie) => {
    if (err) {
      return res.json({ success: false, message: "Some Error" });
    }
    if (movie.length) {
      return res.json({
        success: true,
        message: "Movie fetched by id successfully",
        movie
      });
    } else {
      return res.json({
        success: false,
        message: "Movie with the given id not found"
      });
    }
  });
};

// Returns: error/success message
export const deleteMovie = (req, res) => {
  Movie.findByIdAndRemove(req.params.id, (err, movie) => {
    if (err) {
      return res.json({ success: false, message: "Some Error" });
    }
    return res.json({
      success: true,
      message: movie.Title + " deleted successfully"
    });
  });
};
