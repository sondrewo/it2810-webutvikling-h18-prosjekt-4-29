import mongoose from "mongoose";
import Movie from "../models/movie.server.model";

/**
 * What: Operations for sorting and paging of movies
 *
 * How: Based on recieved paramters from REST, use Mongoose to sort movies in DB.
 * Use QUICK MATHS to page result set into fitting sizes. /Dynamic loading of page-content
 *
 * Returns: Sorted movies
 */

export const sortMovies = (req, res) => {
  let request = {};
  if (req.params.text !== "None") {
    let words = req.params.text.split(" ");
    let firstWord = words[0].toLowerCase();
    if (
      words.length > 1 &&
      (firstWord === "director" || firstWord === "cast")
    ) {
      let regex = new RegExp(words.slice(1).join(" "), "i");
      request[firstWord + ".name"] = regex;
    } else {
      let regex = new RegExp(req.params.text, "i");
      request["title"] = regex;
    }
  }
  if (req.params.year !== "None") {
    request["year"] = req.params.year;
  }
  if (req.params.genre !== "None") {
    request["genres"] = req.params.genre;
  }

  console.log("\n\nRequest:");
  console.log(request);

  let limit = 0;
  let skip = 0;

  if (req.params.page > 0) {
    limit = 15;
    skip = (req.params.page - 1) * 15;
  }

  let query = {};

  let sortDir = 1;
  if (req.params.sortBy.charAt(0) == "-") {
    sortDir = -1;
    req.params.sortBy = req.params.sortBy.substring(1);
  } else if (req.params.sortBy.charAt(0) == "+") {
    req.params.sortBy = req.params.sortBy.substring(1);
  }

  if (req.params.sortBy === "vote_average") {
    query[req.params.sortBy] = -sortDir;
  } else {
    query[req.params.sortBy] = sortDir;
  }

  Movie.find(request)
    .limit(limit)
    .skip(skip)
    .sort(query)
    .exec((err, movie) => {
      if (err) {
        return res.json({ success: false, message: "Some Error" });
      }
      if (movie.length) {
        return res.json({
          success: true,
          message: "Movies sorted successfully",
          movie
        });
      } else {
        return res.json({
          success: false,
          message: "failed to sort movies"
        });
      }
    });
};
