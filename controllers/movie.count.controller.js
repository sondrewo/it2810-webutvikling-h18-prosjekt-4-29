import mongoose from "mongoose";
import Movie from "../models/movie.server.model";

export const countMovies = (req, res) => {
  let request = {};
  if (req.params.text !== "None") {
    let words = req.params.text.split(" ");
    let firstWord = words[0].toLowerCase();
    if (
      words.length > 1 &&
      (firstWord === "director" || firstWord === "cast")
    ) {
      let regex = new RegExp(words.slice(1).join(" "), "i");
      request[firstWord + ".name"] = regex;
    } else {
      let regex = new RegExp(req.params.text, "i");
      request["title"] = regex;
    }
  }
  if (req.params.year !== "None") {
    request["year"] = req.params.year;
  }
  if (req.params.genre !== "None") {
    request["genres"] = req.params.genre;
  }

  if (
    req.params.text == "None" &&
    req.params.genre == "None" &&
    req.params.year == "None"
  ) {
    //Raskere å sjekke uten query hvis den er tom uansett
    Movie.estimatedDocumentCount().exec((err, count) => {
      if (err) {
        return res.json({ success: false, message: "Some Error" });
      } else {
        console.log("empty");
        return res.json({
          success: true,
          message: "Movies sorted successfully",
          count
        });
      }
    });
  } else {
    Movie.countDocuments(request).exec((err, count) => {
      if (err) {
        return res.json({ success: false, message: "Some Error" });
      } else {
        return res.json({
          success: true,
          message: "Movies sorted successfully",
          count
        });
      }
    });
  }
};
