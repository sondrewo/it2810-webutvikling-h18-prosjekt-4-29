/// <reference types="Cypress" />

context("Network Requests", () => {
  it("cy.request()", () => {
    cy.request(
      "http://localhost:3001/sort/original_title/the/None/None/None/0"
    ).should(response => {
      expect(response.status).to.eq(200);
      expect(response.body.movie[0]).to.have.property("year");
      expect(response.body.movie[0]).to.have.property("title");
    });
  });

  let theNun = {};

  it("Get movie object and check properties", () => {
    cy.request(
      "http://localhost:3001/sort/original_title/the nun's story/None/None/None/0"
    ).should(response => {
      theNun = response.body;
      theNun.movie[0].feedbackList.push({
        name: "Action-havard",
        rating: 4,
        review: "sugde"
      });
      expect(response.status).to.eq(200);
      expect(response.body.movie).to.have.length(1);
      expect(response.body.movie[0]).to.have.property("title");
      expect(response.body.movie[0].title).to.eq("The Nun's Story");
    });
  });

  it("Add feedback and check if added", () => {
    cy.request(
      "POST",
      "http://localhost:3001/api/update",
      theNun.movie[0]
    ).should(response => {
      let length = theNun.movie[0].feedbackList.length;
      expect(response.status).to.eq(200);
      expect(response.body.movie.feedbackList[0]).to.have.property("name");
      expect(response.body.movie.feedbackList[length - 1].name).to.eq(
        "Action-havard"
      );
    });
  });

  it("Delete feedback and check if deleted", () => {
    let length = theNun.movie[0].feedbackList.length;
    theNun.movie[0].feedbackList.pop();
    cy.request(
      "POST",
      "http://localhost:3001/api/update",
      theNun.movie[0]
    ).should(response => {
      expect(response.status).to.eq(200);
      expect(response.body.movie.feedbackList).to.have.length(length - 1);
    });
  });

  let ratingSortedMovies = {};
  it("Sort movies by vote average", () => {
    cy.request(
      "http://localhost:3001/sort/vote_average/love/None/None/None/0"
    ).should(response => {
      ratingSortedMovies = response.body.movie;
      let highestRanked = ratingSortedMovies[0].vote_average;
      let secondHighestRanked = ratingSortedMovies[1].vote_average;
      expect(highestRanked).to.be.above(secondHighestRanked);
    });
  });

  let titleSortedMovies = {};
  it("Resort movies by title", () => {
    cy.request("http://localhost:3001/sort/title/love/None/None/None/0").should(
      response => {
        titleSortedMovies = response.body.movie;
        expect(ratingSortedMovies).not.to.eq(titleSortedMovies);
        let result1 = titleSortedMovies[0].title;
        let result2 = titleSortedMovies[1].title;
        let result3 = titleSortedMovies[2].title;
        expect(result2).to.be.above(result1);
        expect(result3).to.be.above(result1);
        expect(result3).to.be.above(result2);
      }
    );
  });
});
