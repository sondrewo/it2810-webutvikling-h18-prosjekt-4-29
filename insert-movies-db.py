'''
Fetches list of movies from The Movie Database API - TMDb,
Goes through the list and fetches more data about the movie from a different TMDb request.
Replaces votecount and vote_average with data from iMDB using imdbpy.

Goes through the most popular movies from the last 100 years
and updates the database if movie isn't already added. 


Filters:
- votecount over 1000      
- if rating over 8, votecount over 25000. 
- Runtime over 60 minutes
- Not documentary og TV Movie

Saves ID's for movies matching filter in .txt-file, so it doesn't 
have to re-check skipped movies next time the script runs.

'''

import sys
import json
import requests
import pymongo
from imdb import IMDb  #imdbpy

ia = IMDb()

#Open file with list over which ID's to skip and saves to array.
with open("skip.txt") as f:
    skipList = f.readlines()
skipList = [int(x.strip()) for x in skipList]
skip = open("skip.txt", "a")

#Connect to database
connection = pymongo.MongoClient('it2810-29.idi.ntnu.no', 27017)
db = connection["p4"]
db.authenticate("ihsadmin", "heieh")
moviesDB = db["movies"]


def movieExistInDatabase(movie):
    return moviesDB.find({"_id": movie["id"]}).limit(1).count() == 1


def getIdNumbers(streng):
    for i in range(len(streng)):
        if streng[i].isdigit():
            return (int(streng[i:]))


#Extract names and ids from cast og crew
def getNameAndId(liste, director):
    names = []
    for c in liste:
        if not director or c["job"] == "Director":
            names.append({
                "id": c["id"],
                "name": c["name"],
            })
    return names


#Remove unnecissary information from object and convert array
def replaceWithArray(movie, object, name):
    array = []
    for item in movie[object]:
        array.append(item[name])
    movie[object] = array


#get more information from found id
def getMovieFromID(id):
    movie = json.loads(
        requests.
        get('https://api.themoviedb.org/3/movie/' + str(id) +
            '?api_key=ae9620f33901db0649c9d3a60ac7859b&language=en-US').text)
    if movie["runtime"] and movie["runtime"] < 60:
        return None

    credits = json.loads(
        requests.get('https://api.themoviedb.org/3/movie/' + str(id) +
                     '/credits?api_key=ae9620f33901db0649c9d3a60ac7859b').text)

    movie["_id"] = movie.pop("id")

    replaceWithArray(movie, "genres", "name")
    replaceWithArray(movie, "production_companies", "name")
    replaceWithArray(movie, "production_countries", "name")
    replaceWithArray(movie, "spoken_languages", "name")

    if "Documentary" in movie["genres"] or "TV Movie" in movie["genres"]:
        return None

    movie["year"] = movie["release_date"][:4]
    movie["director"] = getNameAndId(credits["crew"], True)
    movie["cast"] = getNameAndId(credits["cast"], False)

    if movie["imdb_id"]:
        id = getIdNumbers(movie["imdb_id"])
        mov = ia.get_movie(id)
        if "rating" in mov:
            movie["vote_average"] = mov["rating"]
            movie["vote_count"] = mov["votes"]
    else:
        print("ikke id i ", movie["title"])
    if movie["vote_count"] < 1000 or movie["vote_average"] == 0 or movie[
            "vote_average"] > 8 and movie["vote_count"] < 25000:
        return None
    return movie


#Insert movies

added = 0

pages = 10
years = 100

try:
    for page in range(1, pages + 1):
        print("\n\n\n\n\n")
        if page == 1:
            print("--- Fetching and inserting ---")
        print("---         Page", page, "        ---")
        for year in range(2018, 2018 - years, -1):
            print("Fetching movies from", year, "   Done:",
                  (page - 1) * 2000 + (2018 - year) * 20, "/",
                  pages * years * 20)
            jsonFilmer = []
            movies = json.loads(
                requests.
                get("https://api.themoviedb.org/3/discover/movie?api_key=ae9620f33901db0649c9d3a60ac7859b&language=en-US&sort_by=popularity.desc&page="
                    + str(page) + "&primary_release_year=" +
                    str(year)).text)["results"]
            for movie in movies:
                if movie["id"] not in skipList and not movieExistInDatabase(
                        movie):
                    print("   -" + movie["title"], end="")
                    fetchedMovie = getMovieFromID(movie["id"])
                    if fetchedMovie:
                        jsonFilmer.append(fetchedMovie)
                        print("")
                    else:
                        skip.write("\n" + str(movie["id"]))
                        print(
                            "-     Skipped, low votecount/runtime. Added to skiplist."
                        )
            if len(jsonFilmer) > 0:
                print("Inserting...")
                added += len(jsonFilmer)
                moviesDB.insert_many(jsonFilmer)
                print("Added", len(jsonFilmer), "movies from", year,
                      "to database\n\n\n")
        print("\n\n\n Total: added:", added, "movies")
except KeyboardInterrupt:
    print('\n\n\n\n------------\n\nADDED', added,
          'MOVIES TO DATABASE\n\n------------\n\n')
    sys.exit(0)
