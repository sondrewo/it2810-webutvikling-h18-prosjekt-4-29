# Applikasjonen

Vår løsning tilbyr brukere muligheten til å søke blant filmer lagret i en database. Her kan brukeren videre filtrere og sortere resultatsettet basert på noen parametere. Filmobjektene er lagret på en virituell maskin som ligger innenfor NTNU sitt nett, og det er derfor nødvendig å bruke VPN om man ikke sitter på eduroam. For å sette sammen filmobjektene har vi brukt to eksterne APIér, fra henholdsvis IMDBpy og MoviesDB. Selve innsettingen av filmer skjer i et python-script som kan finnes i prosjektets rotmappe. [script](https://gitlab.stud.idi.ntnu.no/sondrewo/it2810-webutvikling-h18-prosjekt-4-29/blob/master/insert-movies-db.py)

### Setup

For å kjøre prosjektet kreves det en 'npm install' i både tjener og klient-prosjektene.

- `root/npm install`
- `root/moviesearch/npm install`

### Teknologistack

- MongoDB: Installert på gruppens hjemmeområde hos NTNU
- Mongoose: Driver for oppkobling mot MongoDB
- Express & Node: Server, REST API etc..
- React & Redux: Frontend, state-behandling etc..

## Tredjepartskomponenter og API:

I prosjektet har vi brukt flere tredjepartskomponenter og API;

- Material UI : For enkel styling og plassering av UI-komponenter
- react-star-ratings: For visuell fremstilling av stjerner for rating av filmer
- Cypress: For enkel E2E-testing
- cypress-react-unit-test: For enhetstesting med cypress
- IMDBpy API: For å hente filminformasjon
- MoviesDB API: For å hente filminformasjon
- MaterialIcon: For ikoner
- react-easy-chart: For visuell fremstilling av lastet data i stolpediagram-form.

## Funksjonalitet

### Søk og sortering av filmer

Resultatsettet med filmer som hentes er alltid sortert etter ett kriterie: enten tittel, utgivelsesdato eller rating. Vi har valgt IMDB-rating som default. Vi har valgt å sortere på tjenersiden av applikasjonen og gjør dette ved hjelp av Mongoose sitt API for søk i MongoDB-databaser.
Når filmer skal hentes fra databasen, foretar vi et søk med via en http-forespørsel i `sortMoviesAction.js`. Denne forespørselen inkluderer alle felter som er relevante for søket, slik som søkttekst, valgte filtre og sorteringsmetode. Forespørslen mottas på tjenersiden, som vil omdirigere forespørselen basert på http-metode og URL. Etter at forespørselen har blitt omdirigert til riktig end-point, vil søket bli utført via Mongoose sitt query API. Dette skjer for eksemepel i `movies.search.controller`. Filmene som blir returnert fra spørringen blir sendt tilbake til der forespørselen startet, for å så bli lagt til i Redux Store. Komponenten vår som fremstiller filmene grafisk vil få beskjed om at Redux Store har blitt oppdatert, og vil så re-rendere for å vise den oppdaterte tilstanden i applikasjonen.

### UI-komponenter for søk, sortering og filtrering av filmer.

Applikasjonen vår lar brukere filtrere filmer etter år og sjanger. Brukeren kan via to dropdown-komponenter velge et år og en sjanger, og vil få ut igjen filmer som matcher dette kravet. Selve filtreringen skjer på tjenersiden ved hjelp av Mongoose-metoden ´find(request)´.

For å håndtere sortering lar vi brukeren velge mellom tre sorteringsmetoder, illustrert ved hjelp av tre knapper.

Søk håndteres med et tekstfelt som lar brukeren skrive inn en søketekst. Denne teksten blir så sjekket opp mot titlene på alle filmene i DB. Støtter både delvis og fullstendig matching.

### Innlasting av data

Vi har cirka 12,000 filmer i databasen, disse blir naturligvis ikke hentet på en gang. Vi har satt på et dynamisk "page" system som henter et sett med filmer basert på hvilken side du befinner deg på i grensesnittet. Dataen blir lagret i en dictionary med sidetall som key. Vi har implementert systemet slik at valgt side, siden før, og siden etter blir hentet. (Å gå til side 3 laster inn innholdet på side 2 og 4 osv...).

### Brukergenerert data

Inne på informasjonssiden til filmene, har du muligheten til å legge til en tilbakemelding av filmen. Du kan vurdere filmen (1-10), med muligheten for å legge til en anmeldelse i tillegg. Hvis en film har fått tilbakemelding vil gjennomsnittet av brukervurderingene bli vist på toppen av informasjonssiden ved siden av IMDb-rating. Anmeldelser blir listet kronologisk på bunnen av siden, sammen med gjennomsnittsvurderingen av anmeldelsene.

### Ekstra

Over resultatsettet kan du trykke på et stolpediagram-ikon som åpner en modal med et stolpediagram som viser hvor mange filmer som er lastet per år. Dette er kun for filmene som er lastet inn på siden akkurat nå. Når modalen åpnes blir diagrammet generert av react-easy-chart og data fra movies-listen.

## Testing

Vi har brukt testrammeverket Cypress under utviklingen av applikasjonen, både på bakside og framside.

For å kjøre testene:

- Backend: `root/node_modules/.bin/cypress open`
- Frontend: `root/moviesearch/node_modules/.bin/cypress open`

### Kjente feil

En av end-to-end-testene passerer ikke i noen tilfeller grunnet 'Maximum call stack size exceeded'. Samme test vil også feile i noen tilfeller hvis den kjøres i et test-batteri sammen med resten av testene. Grunnen til dette er at cypress renderer raskere enn den tester, så UIét er allerede rendert forbi det punktet den er ment å teste. Dette er dessverre ikke mulig å få gjort noe med, men hvis testen kjøres alene går det fint.

Når du er inne på informasjonssiden til en film, kan du trykke på årstallet for å laste filmer fra dette året. Dette oppdaterer ikke dropdownen for filtrering av år.

### Testing av API og CRUD-operasjoner

Backendtestingen er gjort i `movies_requests.spec.js`. Her foretar vi forskjellige http-forespørsler til backenden vår, hvor vi så sjekker dette resultatet mot forventede resultater. For eksempel har vi noen metoder som tester sorteringen vår, hvor vi sender en get-forespørsel etter filmer som inneholder “love” og at resultatsettet skal sorteres etter tittel/alfabetisk. Da sjekker vi dette ved å se om tittelen til film nr 1 i resultatsettet er mindre enn film nr 2 (man kan sammenligne stringer på samme måte som tall - bokstaver som kommer først i alfabetet vil ha mindre verdi enn de som kommer senere, A<B). Vi tester også andre CRUD-metoder slik som “Update” ved å sende en POST-request med et oppdatert objekt mot en Film ID, for å så kontrollere at objektet blir oppdatert.

### End-to-end testing, snapshot-testing og enhetstesting

I klinet-prosjektet “moviesearch” er det også installert cypress. Under mappen “Cypress/integration/ ligger det en rekke tester som kontrollerer at tilstanden til applikasjonen er slik den er ment å være. I testene subfixet med -component testes det at enkelte av komponentene våre renderer korrekt (snapshot-testing). I testene subfixed med “E2E” gjør vi komplette end-to-end tester. Hver testfil inneholder en beskrivelse av testens formål, så vel som prosedyre og forventet resultat. En av disse E2E-testene kjører opp en lokal instans av applikasjonen, for å så trykke seg gjennom en test-prosedyre som forsøker å legge til en tilbakemelding på en film. Deretter kontrolleres det at denne tilbakemeldingen faktisk ble lagt til på korrekt element (film).
