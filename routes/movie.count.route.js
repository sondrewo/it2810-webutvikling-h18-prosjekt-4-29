import express from "express";
import * as movieCountController from "../controllers/movie.count.controller";

// get an instance of express router and send request to movie controller functions
const router = express.Router();
router.route("/:text/:year/:genre/").get(movieCountController.countMovies);

export default router;
