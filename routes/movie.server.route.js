import express from "express";
/**
 * What: Create router for REST. Redirects request to CRUD method based on http-method.
 *
 * How: Use express to get an instance of the server, and redirect incomming requests.
 *
 */

import * as movieController from "../controllers/movie.server.controller";

const router = express.Router();
router.route("/update").post(movieController.updateMovie);
router
  .route("/:id")
  .get(movieController.getMovie)
  .delete(movieController.deleteMovie);

export default router;
