/**
 * What: Create router for REST. Redirects request to CRUD method based on http-method.
 *
 * How: Use express to get an instance of the server, and redirect incomming requests.
 *
 */

import express from "express";
import * as movieSortController from "../controllers/movie.sort.controller";

const router = express.Router();
router
  .route("/:sortBy/:text/:year/:genre/:page")
  .get(movieSortController.sortMovies);

export default router;
