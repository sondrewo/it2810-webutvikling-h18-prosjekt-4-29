/**
 * What: Create router for REST. Redirects request to CRUD method based on http-method.
 *
 * How: Use express to get an instance of the server, and redirect incomming requests.
 *
 */

import express from "express";
import * as movieSearchController from "../controllers/movie.search.controller";

const router = express.Router();
router.route("/:text/:year/:genre").get(movieSearchController.getMovie);

export default router;
