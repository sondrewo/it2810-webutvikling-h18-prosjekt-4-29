/*
Snapshot-test  for HeaderComponent, check that it renders

Expected behaviour: Component renders without errors
*/

import { HeaderComponent } from "../../src/components/HeaderComponent";
import React from "react";
import { mount } from "cypress-react-unit-test";

describe("HeaderComponent", () => {
  it("works", () => {
    mount(<HeaderComponent />);
    cy.contains("Movie Search");
  });
});
