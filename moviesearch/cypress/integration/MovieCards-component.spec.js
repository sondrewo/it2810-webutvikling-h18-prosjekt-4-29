/*
Snapshot-test for MovieCards Component, check that it renders 

Expected behaviour: Component renders without errors
*/

import { MovieCards } from "../../src/components/MovieCards";
import React from "react";
import { mount } from "cypress-react-unit-test";

describe("MovieCards", () => {
  it("works", () => {
    mount(<MovieCards movies={[]} />);
  });
});
