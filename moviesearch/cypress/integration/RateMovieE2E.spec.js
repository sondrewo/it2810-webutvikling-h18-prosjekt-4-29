/*

E2E test for reviewing a movie.
Test controlls that the UI is sending the correct requests to the API. 

1. Select input field and search for "The Nun"
2. If correct requests are sent, movies will be returned 
- which of at least on of them will have the title "The Nun". 
3. Click on the movie object 
4. Click "Add review"
5. Enter input field for name and review
6. Give a star rating (Max score)
7. Post review 
8. Verify that review lists updates with the new review

Expected behaviour: The movie "The Nun" is rated and reviewed, 
-> page updates with the new review in list

*/

describe("RateMovieE2E", () => {
  it("Visit landing page", () => {
    cy.visit("/");
    cy.get("#standard-full-width")
      .type("The Nun")
      .type("{enter}");
    cy.get('[title="The Nun"]').click();
    cy.get('[id="reviewButton"]').click();
    cy.get("#name-input").type("Test Bot");
    cy.get("[viewBox='0 0 51 48']").click({ multiple: true });
    cy.get("#review-input").type("Middels bra");
    cy.get('[id="reviewButton"]').click();
    cy.contains("TestBot");
  });
});
