/*

E2E test for Search and selection of movie.
Test controlls that the UI is sending the correct requests to the API. 

1. Select input field and search for "The Nun"
2. If correct requests are sent, movies will be returned 
- which of at least on of them will have the title "The Nun". 
3. Click on the movie object and verify that the title and year of that movie is present

Expected behaviour: The movie "The Nun" is selected

*/

describe("SearchForMovieE2E", () => {
  it("Visit landing page", () => {
    cy.visit("/");
    cy.get("#standard-full-width")
      .type("The Nun")
      .type("{enter}");
    cy.get('[title="The Nun"]').click();
    cy.contains("The Nun");
    cy.contains("2018");
  });
});
