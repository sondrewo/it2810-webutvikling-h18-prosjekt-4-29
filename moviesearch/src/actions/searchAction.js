import { fetchMovies } from "./fetchMoviesAction";

export function search(
  sortBy,
  text = "None",
  year = "None",
  genre = "None",
  pages = [1, 2]
) {
  return function(dispatch) {
    dispatch({ type: "FETCH_MOVIES_START" });
    for (var page in pages) {
      /*loads one page at a time, fetch movies */
      dispatch(fetchMovies(text.trim(), year, genre, sortBy, pages[page]));
    }
  };
}
