export function fetchMovies(text, year, genre, sortBy, page = 0) {
  return function(dispatch) {
    if (page <= 1) {
      dispatch({ type: "CLEAR_MOVIES" });
      dispatch({ type: "RESET" });
    }

    /*send http request and fetch movies from database*/
    fetch(
      "http://localhost:3001/sort/" +
        sortBy +
        "/" +
        text +
        "/" +
        year +
        "/" +
        genre +
        "/" +
        page
    )
      /*handle received payload */
      .then(response => response.json())
      .then(data => {
        dispatch({ type: "RECIEVE_MOVIES", payload: data.movie, page: page });
      })
      .then(() => {
        if (page <= 1) {
          fetch(
            "http://localhost:3001/count/" + text + "/" + year + "/" + genre
          )
            .then(response => response.json())
            .then(data => {
              dispatch({ type: "SET_RESULTS_COUNT", payload: data.count });
            });
        }
      })
      .catch(err => {
        dispatch({ type: "FAILED_SORT", payload: err });
      });
  };
}
