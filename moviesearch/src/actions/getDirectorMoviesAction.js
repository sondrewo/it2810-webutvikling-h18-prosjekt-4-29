export function getDirectorMovies(director) {
  return function(dispatch) {
    /*sends http request to fetch movies made by specific director */
    fetch(
      "http://localhost:3001/sort/release_date/director " +
        director +
        "/None/None/1"
    )
      .then(response => response.json())
      .then(data => {
        dispatch({
          type: "RECEIVE_DIRECTOR_MOVIES",
          payload: data.movie
        });
      })

      .catch(err => {
        dispatch({ type: "FAILED_SORT", payload: err });
      });
  };
}
