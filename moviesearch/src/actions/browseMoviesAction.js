/*when viewing moviedetails (after pressing a movie), the user can use the arrowkeys to navigate
to and view the previous and next movie*/
export function nextMovie(movie, movies) {
  return function(dispatch) {
    for (let i = 0; i < movies.length - 1; i++) {
      if (movies[i] === movie) {
        dispatch({ type: "SELECT_MOVIE", selectedMovie: movies[i + 1] });
      }
    }
  };
}

export function prevMovie(movie, movies) {
  return function(dispatch) {
    for (let i = 1; i < movies.length; i++) {
      if (movies[i] === movie && i !== 0) {
        dispatch({ type: "SELECT_MOVIE", selectedMovie: movies[i - 1] });
      }
    }
  };
}
