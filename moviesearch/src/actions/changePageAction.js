import { search } from "././searchAction";

/*action for when user selects next/prev page with movies*/

export function changePage(
  newPage,
  oldPage,
  pageCount,
  movies,
  sortBy,
  text,
  year,
  genre
) {
  return function(dispatch) {
    if (newPage !== oldPage) {
      dispatch({ type: "SET_PAGE", payload: newPage });
      let years = [];
      for (let i = newPage - 1; i <= newPage + 1; i++) {
        if (!movies[i] && i !== 0 && i !== pageCount + 1) {
          years.push(i);
        }
      }
      if (years.length > 0) {
        dispatch(search(sortBy, text, year, genre, years));
      }
    }
  };
}
