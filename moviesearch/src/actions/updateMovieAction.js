export function updateMovie(updatedMovieObject) {
  return function(dispatch) {
    /*when adding review on movie, replace old movie object with new movie and new review*/
    fetch("http://localhost:3001/api/update/", {
      method: "POST", // *GET, POST, PUT, DELETE, etc.
      mode: "cors", // no-cors, cors, *same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      credentials: "same-origin", // include, *same-origin, omit
      headers: {
        "Content-Type": "application/json; charset=utf-8"
        // "Content-Type": "application/x-www-form-urlencoded",
      },
      redirect: "follow", // manual, *follow, error
      referrer: "no-referrer", // no-referrer, *client
      body: JSON.stringify(updatedMovieObject) // body data type must match "Content-Type" header
    }).then(() => {
      dispatch({ type: "ADD_FEEDBACK", updatedMovie: updatedMovieObject });
      dispatch({
        type: "RESET_REVIEW_FIELDS",
        updatedMovie: updatedMovieObject
      });
      dispatch({
        type: "OPEN_REVIEW",
        payload: false
      });
    });
  };
}
