export function setFilters(sort, year, genre) {
  return function(dispatch) {
    /*actions to update state to what filtering options the user has selected*/

    dispatch({ type: "UPDATE_YEAR", payload: sort });
    dispatch({ type: "UPDATE_GENRE", payload: year });
    dispatch({ type: "UPDATE_SORT", payload: genre });
  };
}
