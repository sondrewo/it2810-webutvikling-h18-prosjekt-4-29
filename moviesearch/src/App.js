import React, { Component } from "react";
import "./App.css";
import HeaderComponent from "./components/HeaderComponent";
import SearchComponent from "./components/SearchComponent";
import SelectedMovieComponent from "./components/selected_movie/SelectedMovieComponent";
import { connect } from "react-redux";

class App extends Component {
  render() {
    console.log(this.props);
    return (
      <div className="App" style={container}>
        <HeaderComponent />
        <SearchComponent />
        <SelectedMovieComponent />
      </div>
    );
  }
}

let container = {
  color: "white",
  display: "flex",
  flexDirection: "column",
  alignContent: "center",
  justifyContent: "space-between"
};

const mapStateToProps = state => ({
  selectedMovie: state.selectMovie.selectedMovie,
  openChart: state.chart.openChart
});

export default connect(mapStateToProps)(App);
