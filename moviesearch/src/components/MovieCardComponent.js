import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { connect } from "react-redux";
import RatingComponent from "./RatingComponent";

class MovieCardComponent extends Component {
  /*handles event when user presses specific movie */
  selectMovie = () => {
    this.props.dispatch({
      type: "SELECT_MOVIE",
      selectedMovie: this.props.movie
    });
    this.props.dispatch({
      type: "SHOW_MOVIE_LIST",
      showMovies: false
    });
  };

  /*get list of genres */
  getGenreList(genreList) {
    let genres = "";
    for (let i = 0; i < genreList.length; i++) {
      genres += genreList[i];
      if (i < 2 && i < genreList.length - 1) {
        genres += ", ";
      }
      if (i === 2 && genreList.length > 2) {
        genres += "...";
        break;
      }
    }
    return genres;
  }

  hover = movieID => {
    this.props.dispatch({ type: "HOVER_MOVIE", payload: movieID });
  };

  unhover = () => {
    this.props.dispatch({ type: "UNHOVER_MOVIE" });
  };

  render() {
    return (
      <Card
        style={
          this.props.hoverMovie &&
          this.props.hoveredMovieID === this.props.movie._id
            ? cardhover
            : card
        }
        onClick={this.selectMovie}
        onMouseOver={() => this.hover(this.props.movie._id)}
        onMouseLeave={this.unhover}
      >
        <CardMedia
          style={{ height: "160px", paddingTop: "56.25%" }}
          image={
            "https://image.tmdb.org/t/p/original" + this.props.movie.poster_path
          }
          title={this.props.movie.title}
        />
        <CardContent style={container}>
          <div>
            <Typography variant="subtitle2">
              <div
                style={{
                  color: "white",
                  marginTop: "5px",
                  fontFamily: "Lato"
                }}
              >
                <div style={movieDescription1}> {this.props.movie.year}</div>
                <RatingComponent
                  style={movieDescription2}
                  rating={this.props.movie.vote_average}
                />
              </div>
            </Typography>
          </div>
        </CardContent>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  hoverMovie: state.selectMovie.hoverMovie,
  selectedMovie: state.selectMovie.selectedMovie,
  hoveredMovieID: state.selectMovie.hoveredMovieID
});

export default connect(mapStateToProps)(MovieCardComponent);

let container = {
  position: "relative"
};

let cardhover = {
  width: "188px",
  height: "330",
  padding: "0",
  margin: "10px",
  cursor: "pointer",
  backgroundColor: "#2e3035"
};

let card = {
  width: "188px",
  height: "330px",
  padding: "0",
  margin: "10px",
  cursor: "pointer",
  backgroundColor: "rgba(12,15,18)"
};

let movieDescription1 = {
  display: "inline-block",
  marginTop: "6px"
};

let movieDescription2 = {
  display: "inline-block"
};
