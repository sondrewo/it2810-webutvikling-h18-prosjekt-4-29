import React, { Component } from "react";
import { connect } from "react-redux";
import { FormControl } from "@material-ui/core";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MaterialIcon from "material-icons-react";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { search } from "../actions/searchAction";
import OutlinedInput from "@material-ui/core/OutlinedInput";

const styles = theme => ({
  root: {
    color: "#02FB87",
    display: "flex",
    flexWrap: "wrap",
    borderColor: "white"
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  labelRoot: {
    color: "white"
  }
});

class FilterComponent extends Component {
  /*handles event when user changes filter or sort attribute */
  handleChange = event => {
    let type;
    let year = this.props.selectedYear;
    let genre = this.props.selectedGenre;
    let sort = this.props.sortBy;
    switch (event.target.name) {
      case "year": {
        type = "UPDATE_YEAR";
        year = event.target.value;
        break;
      }
      case "genre": {
        type = "UPDATE_GENRE";
        genre = event.target.value;
        break;
      }
      case "sort": {
        type = "UPDATE_SORT";
        sort = event.target.value;
        break;
      }
      default: {
        type = "RESET";
      }
    }
    this.props.dispatch({ type: type, payload: event.target.value });
    this.props.dispatch(search(sort, this.props.searchText, year, genre));
  };

  render() {
    const { classes } = this.props;
    return (
      <div style={selectParts}>
        <FormControl
          variant="outlined"
          className={classes.formControl}
          style={{
            marginRight: "0.5%",
            minWidth: "4.1em"
          }}
        >
          <InputLabel
            ref={ref => {
              this.InputLabelRef = ref;
            }}
            htmlFor={"year simple"}
            classes={{
              root: classes.labelRoot
            }}
          >
            year
          </InputLabel>
          <Select
            native
            onChange={this.handleChange}
            input={
              <OutlinedInput
                labelWidth={1}
                name={"year"}
                id={"year simple"}
                classes={{
                  root: classes.root
                }}
              />
            }
            classes={{
              root: classes.root
            }}
          >
            {this.props.years.map((y, i) => {
              if (i === 0) {
                return <option key={i} value={"None"} />;
              } else {
                return (
                  <option key={i} value={y}>
                    {y}
                  </option>
                );
              }
            })}
          </Select>
        </FormControl>
        <FormControl
          variant="outlined"
          className={classes.formControl}
          style={{
            marginRight: "0.5%",
            minWidth: "4.1em"
          }}
        >
          <InputLabel
            ref={ref => {
              this.InputLabelRef = ref;
            }}
            htmlFor={"genre simple"}
            classes={{
              root: classes.labelRoot
            }}
          >
            genre
          </InputLabel>
          <Select
            native
            onChange={this.handleChange}
            input={
              <OutlinedInput
                labelWidth={1}
                name={"genre"}
                id={"genre simple"}
                classes={{
                  root: classes.root
                }}
              />
            }
            classes={{
              root: classes.root
            }}
          >
            {this.props.genres.map((y, i) => {
              if (i === 0) {
                return <option key={i} value={"None"} />;
              } else {
                return (
                  <option key={i} value={y}>
                    {y}
                  </option>
                );
              }
            })}
          </Select>
        </FormControl>
        <div style={{ width: "4.1em" }} />
        <FormControl
          variant="outlined"
          className={classes.formControl}
          style={{
            marginRight: "0.5%",
            minWidth: "4.1em"
          }}
        >
          <InputLabel
            ref={ref => {
              this.InputLabelRef = ref;
            }}
            htmlFor={"sort simple"}
            classes={{
              root: classes.labelRoot
            }}
          >
            Sort by
          </InputLabel>
          <Select
            native
            onChange={this.handleChange}
            input={
              <OutlinedInput
                labelWidth={1}
                name={"sort"}
                id={"sort simple"}
                classes={{
                  root: classes.root
                }}
              />
            }
            classes={{
              root: classes.root
            }}
          >
            <option value="vote_average">Rating</option>
            <option value="release_date">Release date</option>
            <option value="title">Alphabetical</option>
          </Select>
        </FormControl>
        <div style={{ cursor: "pointer" }}>
          <MaterialIcon
            onClick={() => {
              this.props.dispatch({ type: "CHANGE_SORT_DIRECTION" });
              this.props.dispatch(
                search(
                  (this.props.sortBy.charAt(0) === "+" ? "-" : "+") +
                    this.props.sortBy.substring(1),
                  this.props.searchText,
                  this.props.selectedYear,
                  this.props.selectedGenre
                )
              );
            }}
            icon="swap_vert"
            color="white"
          />
        </div>
      </div>
    );
  }
}

FilterComponent.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  searchText: state.textField.searchText,
  sortBy: state.sort.sortBy,
  selectedYear: state.yearField.selectedYear,
  selectedGenre: state.genreField.selectedGenre,
  years: state.yearField.years,
  genres: state.genreField.genres
});

export default connect(mapStateToProps)(withStyles(styles)(FilterComponent));

let selectParts = {
  marginTop: "20px",
  display: "flex",
  alignContent: "center",
  justifyContent: "center",
  alignItems: "center"
};
