import React, { Component } from "react";
import StarRatings from "react-star-ratings";

class RatingComponent extends Component {
  render() {
    return (
      <span
        style={{
          float: "right",
          display: "inline-block",
          margin: "2%"
        }}
      >
        <div style={{ display: "flex" }}>
          <div style={{ lineHeight: "1.8em" }}>
            <StarRatings
              rating={1}
              numberOfStars={1}
              starDimension="1.2em"
              starRatedColor="#C39400"
            />
            {this.props.text1 && (
              <div
                style={{
                  fontSize: "0.7em",
                  textAlign: "center",
                  position: "relative",
                  top: "-1.2em"
                }}
              >
                {this.props.text1}
              </div>
            )}
          </div>
          <div style={{ paddingLeft: "3px" }}>
            <span>
              <span style={{ fontSize: "1.2em" }}>{this.props.rating}</span>
              <span style={{ fontSize: "0.8em", color: "rgb(200,200,200)" }}>
                /10
              </span>
              <br />
            </span>
            <div
              style={{
                fontWeight: 50,
                fontSize: "0.7em",
                position: "relative",
                color: "rgb(200,200,200)"
              }}
            >
              <span>{this.props.text2}</span>
            </div>
          </div>
        </div>
      </span>
    );
  }
}
export default RatingComponent;
