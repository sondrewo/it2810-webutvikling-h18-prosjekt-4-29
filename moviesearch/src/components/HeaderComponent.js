import React, { Component } from "react";
import { connect } from "react-redux";
import { search } from "../actions/searchAction";

class HeaderComponent extends Component {
  render() {
    return (
      <div>
        <h1
          style={headerStyle}
          onClick={() => {
            this.props.dispatch({
              type: "UPDATE_SORT",
              payload: "+vote_average"
            });
            this.props.dispatch(search("vote_average"));
          }}
        >
          Movie Search
        </h1>
      </div>
    );
  }
}

export default connect()(HeaderComponent);

let headerStyle = {
  cursor: "pointer",
  color: "rgb(255,255,255)",
  textShadow: "3px 3px 5px #000000",
  fontSize: "450%",
  textAlign: "center",
  fontFamily: "Lato",
  fontWeight: "bold"
};
