import React, { Component } from "react";

import InputFieldComponent from "./InputFieldComponent";
import FilterComponent from "./FilterComponent";
import PageViewerComponent from "./PageViewerComponent";
import MovieCards from "./MovieCards";
import SearchInfoComponent from "./SearchInfoComponent";
import ChartComponent from "./ChartComponent";
import { connect } from "react-redux";
import { search } from "../actions/searchAction";

class SearchComponent extends Component {
  /*initial search for movies first time loading page*/
  componentDidMount() {
    this.handleSearch("vote_average");
    var interval = setInterval(() => {
      if (!this.props.fetched) {
        this.handleSearch("vote_average");
      } else {
        clearInterval(interval);
      }
    }, 1000);
  }

  /*handles event where user presses enter key or search button */
  handleKeypress = target => {
    if (target.charCode === 13) {
      this.handleSearch();
    }
  };

  /*calls action that fetched movies from database*/
  handleSearch = () => {
    this.props.dispatch(
      search(
        this.props.sortBy,
        this.props.searchText,
        this.props.selectedYear,
        this.props.selectedGenre
      )
    );
  };

  render() {
    return (
      <div style={container}>
        <InputFieldComponent
          handleKeypress={this.handleKeypress}
          search={this.handleSearch}
        />
        <SearchInfoComponent />
        <FilterComponent />

        {this.props.openChart && <ChartComponent />}
        <PageViewerComponent />
        {this.props.fetching && (
          <i style={loadingSymbol} className="fas fa-spinner fa-spin" />
        )}
        <MovieCards />
      </div>
    );
  }
}

let container = {
  color: "white",
  display: "flex",
  flexDirection: "column",
  alignContent: "center",
  justifyContent: "center"
};
let loadingSymbol = {
  display: "flex",
  alignContent: "center",
  justifyContent: "center",
  marginTop: "40px"
};

const mapStateToProps = state => ({
  movies: state.search.movies,
  page: state.pageReducer.page,
  selecterMovie: state.search.selectedMovie,
  sortBy: state.sort.sortBy,
  searchText: state.textField.searchText,
  selectedYear: state.yearField.selectedYear,
  selectedGenre: state.genreField.selectedGenre,
  years: state.yearField.years,
  genres: state.genreField.genres,
  openChart: state.chart.openChart,
  fetching: state.search.fetching,
  fetched: state.search.fetched
});

export default connect(mapStateToProps)(SearchComponent);
