import React, { Component } from "react";
import MovieCardComponent from "./MovieCardComponent";
import MaterialIcon from "material-icons-react";
import { connect } from "react-redux";

class MovieCards extends Component {
  render() {
    return (
      <div style={movieCardsContainer}>
        {this.props.returnedNull ? (
          <p>
            <br />
            No movies found!
          </p>
        ) : (
          <div>
            {this.props.movies[this.props.page] && (
              <div>
                <span style={{ color: "grey", marginLeft: "1%" }}>
                  {"Returned " +
                    this.props.resultsCount +
                    " movies matching your filters. "}
                  <span
                    style={{
                      cursor: "pointer",
                      verticalAlign: "bottom",
                      lineHeight: "2em"
                    }}
                  >
                    <MaterialIcon
                      onClick={() =>
                        this.props.dispatch({ type: "OPEN_CHART" })
                      }
                      icon="insert_chart"
                      color="white"
                    />
                  </span>
                </span>
                <div style={movieCards}>
                  {/*iterates over fetched movies and creates a new movie card for each movie*/}
                  {Object.keys(this.props.movies).length > 0
                    ? this.props.movies[this.props.page].map(movie => (
                        <MovieCardComponent movie={movie} key={movie._id} />
                      ))
                    : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(() => (
                        <MovieCardComponent />
                      ))}
                </div>
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  returnedNull: state.search.returnedNull,
  page: state.pageReducer.page,
  movies: state.search.movies,
  resultsCount: state.search.resultsCount
});

export default connect(mapStateToProps)(MovieCards);

let movieCardsContainer = {
  display: "flex",
  alignContent: "center",
  justifyContent: "center",
  margin: "40px 0"
};

let movieCards = {
  maxWidth: "1100px",
  display: "flex",
  justifyContent: "flex-start",
  flexDirection: "row",
  flexWrap: "wrap"
};
