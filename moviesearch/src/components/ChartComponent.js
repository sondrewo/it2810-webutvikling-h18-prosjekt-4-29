import React, { Component } from "react";
import { connect } from "react-redux";
import Modal from "@material-ui/core/Modal";
import { BarChart } from "react-easy-chart";

class Chartcomponent extends Component {
  /*creates chart over nr of movies that belongs to specific year */
  componentDidMount() {
    let newChart = [];
    let years = {};

    for (var key in this.props.movies) {
      this.props.movies[key].forEach(movie => {
        if (!years[movie.year]) {
          years[movie.year] = 1;
        }
        years[movie.year] += 1;
      });
    }
    for (let item in years) {
      newChart.push({ x: item, y: years[item] });
    }

    this.props.dispatch({ type: "UPDATE_CHART", payload: newChart });
  }

  render() {
    return (
      <div>
        {" "}
        <Modal
          open={true}
          onBackdropClick={() => this.props.dispatch({ type: "CLOSE_CHART" })}
          onEscapeKeyDown={() => this.props.dispatch({ type: "CLOSE_CHART" })}
        >
          <div style={chartModal}>
            <BarChart
              axes
              colorBars
              height={500}
              width={1000}
              data={this.props.chartData}
              style={{ margin: "auto" }}
            />

            <span>
              {" "}
              Loaded movies: {Object.keys(this.props.movies).length * 15}/
              {Math.ceil(this.props.resultsCount)}
            </span>
          </div>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  movies: state.search.movies,
  resultsCount: state.search.resultsCount,
  chartData: state.chart.chartData,
  openChart: state.chart.openChart
});

let chartModal = {
  position: "absolute",
  left: "50%",
  top: "50%",
  transform: "translate(-50%, -50%)",
  backgroundColor: "white",
  outline: "none"
};

export default connect(mapStateToProps)(Chartcomponent);
