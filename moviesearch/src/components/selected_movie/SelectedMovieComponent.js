import React, { Component } from "react";
import Modal from "@material-ui/core/Modal";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { nextMovie, prevMovie } from "../../actions/browseMoviesAction";
import MovieDetailsContent from "./MovieDetailsContent";
import MovieListComponent from "./MovieListComponent";
import MaterialIcon from "material-icons-react";

class SelectedMovieComponent extends Component {
  /*handles event when user presses arrow keys to navigate between movies */
  handleKeyPress = event => {
    switch (event.key) {
      case "ArrowRight": {
        this.props.dispatch(
          nextMovie(
            this.props.selectedMovie,
            this.props.movies[this.props.page]
          )
        );
        break;
      }
      case "ArrowLeft": {
        this.props.dispatch(
          prevMovie(
            this.props.selectedMovie,
            this.props.movies[this.props.page]
          )
        );

        break;
      }
      default:
    }
  };

  /*hide movie details */
  hideDetails = () => {
    this.props.dispatch({ type: "UNSELECT_MOVIE" });
    this.props.dispatch({ type: "RESET_REVIEW_FIELDS" });
    this.props.dispatch({ type: "SET_HOVER", payload: false });
    this.props.dispatch({ type: "SHOW_MOVIE_LIST", showMovieList: false });
  };
  hover = () => {
    this.props.dispatch({ type: "SET_HOVER", payload: true });
  };
  unHover = () => {
    this.props.dispatch({ type: "SET_HOVER", payload: false });
  };

  render() {
    let movie = this.props.selectedMovie;
    const { classes } = this.props;

    return (
      <Modal
        open={movie != null}
        onBackdropClick={this.hideDetails}
        onEscapeKeyDown={this.hideDetails}
      >
        <div className={classes.paper} onKeyDown={this.handleKeyPress}>
          <div
            className={classes.exitButton}
            onClick={this.hideDetails}
            onMouseEnter={this.hover}
            onMouseLeave={this.unHover}
          >
            <MaterialIcon
              key={this.props.hover ? 1 : 2}
              icon="close"
              color={this.props.hover ? "white" : "rgb(50,50,50"}
              size={20}
            />
          </div>
          {movie !== null && (
            <img
              src={"https://image.tmdb.org/t/p/original" + movie.backdrop_path}
              width={"100%"}
              alt={movie.title}
              className={classes.backdrop}
            />
          )}
          {movie != null && !this.props.showMovies ? (
            <MovieDetailsContent />
          ) : (
            <MovieListComponent />
          )}
        </div>
      </Modal>
    );
  }
}

SelectedMovieComponent.propTypes = {
  classes: PropTypes.object.isRequired
};

const styles = theme => ({
  paper: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%) !important",
    width: "85%",
    height: "auto",
    minHeight: "75%",
    backgroundColor: "black",
    boxShadow: theme.shadows[5],
    margin: "0 auto",
    outline: "none"
  },
  exitButton: {
    cursor: "pointer",
    height: "20px",
    width: "20px",
    zIndex: 5,
    position: "absolute",
    right: "20%",
    top: "14%",
    borderRadius: "50%",
    textAlign: "center",
    transform: "translate(30%, 80%) !important",
    backgroundColor: "#02FB87",
    boxShadow: "-2px 2px 3px"
  },
  backdrop: {
    opacity: "0.8",
    filter: "blur(3px)"
  }
});

const mapStateToProps = state => ({
  movies: state.search.movies,
  selectedMovie: state.selectMovie.selectedMovie,
  showMovies: state.showMovies.showMovies,
  page: state.pageReducer.page,
  hover: state.closeButton.hover
});

export default connect(mapStateToProps)(
  withStyles(styles)(SelectedMovieComponent)
);
