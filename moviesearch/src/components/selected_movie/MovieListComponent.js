import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import MovieCardComponent from ".././MovieCardComponent";
import { getDirectorMovies } from "../../actions/getDirectorMoviesAction";

class MovieListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  /*shows movies that specific director has directed*/
  componentDidMount() {
    this.props.dispatch(
      getDirectorMovies(this.props.selectedMovie.director[0].name)
    );
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.movieCard}>
        {this.props.directorMovies.slice(0, 10).map(movie => (
          <MovieCardComponent movie={movie} key={movie._id} />
        ))}
      </div>
    );
  }
}

MovieListComponent.propTypes = {};

const styles = theme => ({
  movieCard: {
    color: "white",
    fontFamily: "Oswald",
    fontSize: "1em",
    backgroundColor: "rgba(0,0,0,0.75)",
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%) !important",
    width: "60%",
    height: "66%",
    overflowY: "scroll",
    outline: "none",
    display: "flex",

    justifyContent: "center",
    flexDirection: "row",
    flexWrap: "wrap"
  },

  backdrop: {
    opacity: "0.8",
    filter: "blur(3px)"
  },
  exitButton: {
    cursor: "pointer",
    height: "20px",
    width: "20px",
    zIndex: 5,
    position: "absolute",
    right: "20%",
    top: "14%",
    borderRadius: "50%",
    textAlign: "center",
    transform: "translate(30%, 80%) !important",
    backgroundColor: "#02FB87",
    boxShadow: "-2px 2px 3px"
  }
});

const mapStateToProps = state => ({
  movies: state.search.movies,
  selectedMovie: state.selectMovie.selectedMovie,
  directorMovies: state.search.directorMovies
});

export default connect(mapStateToProps)(withStyles(styles)(MovieListComponent));
