import React, { Component } from "react";
import StarRatings from "react-star-ratings";

class ReviewsComponent extends Component {
  render() {
    return (
      <div>
        <span>REVIEWS</span>
        {this.props.feedbackList.length > 0 && (
          <span>
            <span style={{ color: "rgb(200,200,200)" }}>
              {" (" + this.props.feedbackList.length})
            </span>
            <span style={{ float: "right" }}>
              <span style={{ fontSize: "0.8em" }}>
                Average reviewer rating:
              </span>
              &nbsp;
              <StarRatings
                rating={1}
                numberOfStars={1}
                starDimension="20px"
                starRatedColor="#C39400"
              />
              {this.props.averageUserScore}
              <span style={{ fontSize: "0.65em" }}>/10</span>
            </span>
          </span>
        )}
        <hr style={{ marginTop: 3 }} size={2} />
        <span>
          {this.props.feedbackList.length > 0 ? (
            this.props.feedbackList
              .map((feedBack, index) => (
                <div>
                  {this.props.feedbackList.length !== index + 1 && (
                    <hr size={1} />
                  )}
                  <span
                    style={{
                      fontSize: "0.8em",
                      color: "rgb(200,200,200)"
                    }}
                  >
                    Review by:{" "}
                    <span style={{ textDecoration: "underline" }}>
                      {feedBack["name"]}
                    </span>
                  </span>
                  <span style={{ float: "right" }}>
                    <StarRatings
                      rating={1}
                      numberOfStars={1}
                      starDimension="20px"
                      starRatedColor="#C39400"
                    />
                    {feedBack["rating"]}
                    <span style={{ fontSize: "0.65em" }}>/10</span>
                  </span>
                  <br />
                  <span>{feedBack["review"]} </span> <br />
                  <br />
                </div>
              ))
              .reverse()
          ) : (
            <span style={{ color: "rgb(200,200,200)" }}>
              There are no reviews for this movie, write one!
            </span>
          )}
        </span>
      </div>
    );
  }
}
export default ReviewsComponent;
