import React, { Component } from "react";
import StarRatings from "react-star-ratings";
import { FormControl } from "@material-ui/core";
import InputLabel from "@material-ui/core/InputLabel";
import InputBase from "@material-ui/core/InputBase";
import MaterialIcon from "material-icons-react";
import { connect } from "react-redux";

class AddReviewComponent extends Component {
  /*minimize review window in movie details*/
  hideReview = () => {
    this.props.dispatch({ type: "OPEN_REVIEW", payload: false });
  };

  /*add name of reviewer in state */
  updateName = event => {
    this.props.dispatch({
      type: "UPDATE_NAME",
      name: event.target.value.trim()
    });
  };

  /*add new review to movie object */
  updateReview = event => {
    this.props.dispatch({ type: "UPDATE_REVIEW", review: event.target.value });
  };

  /*updates rating from user */
  changeRating = rating => {
    this.props.dispatch({
      type: "UPDATE_RATING",
      rating: Number(rating)
    });
  };

  render() {
    return (
      <div>
        <div className={this.props.classes.exitForm} onClick={this.hideReview}>
          <MaterialIcon key={1} icon="close" color={"white"} size={20} />
        </div>
        <FormControl
          className={this.props.classes.margin}
          fullWidth="true"
          style={{ margin: "10px 0" }}
        >
          <InputLabel
            shrink
            htmlFor="name-input"
            className={this.props.classes.bootstrapFormLabel}
          >
            Name
          </InputLabel>
          <InputBase
            id="name-input"
            classes={{
              root: this.props.classes.bootstrapRoot,
              input: this.props.classes.bootstrapInput
            }}
            value={this.props.name}
            onChange={this.updateName}
            fullWidth="true"
          />
        </FormControl>
        <span style={{ paddingRight: "25px" }}>Rating</span>

        <StarRatings
          id="rating-input"
          rating={this.props.rating}
          starRatedColor="#C39400"
          starHoverColor="#C39400"
          changeRating={this.changeRating}
          name="rating-input"
          numberOfStars={10}
          starDimension="25px"
          starSpacing="1px"
        />
        <span style={{ float: "right" }}>
          {this.props.rating > 0 ? this.props.rating : "*"}
          /10
        </span>

        <FormControl
          className={this.props.classes.margin}
          fullWidth="true"
          style={{ margin: "10px 0" }}
        >
          <InputLabel
            shrink
            htmlFor="review-input"
            className={this.props.classes.bootstrapFormLabel}
          >
            Review
          </InputLabel>
          <InputBase
            id="review-input"
            classes={{
              root: this.props.classes.bootstrapRoot,
              input: this.props.classes.bootstrapInput
            }}
            multiline="true"
            rows="3"
            onChange={this.updateReview}
            value={this.props.review}
            fullWidth="true"
          />
        </FormControl>
        <br />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  name: state.textField.name,
  rating: state.review.rating,
  review: state.textField.review
});

export default connect(mapStateToProps)(AddReviewComponent);
