import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import { updateMovie } from "../../actions/updateMovieAction";
import { nextMovie, prevMovie } from "../../actions/browseMoviesAction";
import RatingComponent from "./../RatingComponent";
import ReviewsComponent from "./ReviewsComponent";
import AddReviewComponent from "./AddReviewComponent";
import StarRatings from "react-star-ratings";
import { search } from "../../actions/searchAction";

class MovieDetailsContent extends Component {
  constructor(props) {
    super(props);
    this.state = { showMovieList: false };
  }

  /*when pressing a a year in movie-details/popup, page directs you to new page with search on that year */
  searchByYear = () => {
    this.props.dispatch({
      type: "UPDATE_YEAR",
      payload: this.props.selectedMovie.year
    });

    this.props.dispatch({ type: "UPDATE_TEXT", payload: "None" });

    this.hideDetails();
    this.props.dispatch(
      search(this.props.sortBy, "None", this.props.selectedMovie.year)
    );
  };

  changeRating = rating => {
    this.props.dispatch({
      type: "UPDATE_RATING",
      rating: Number(rating)
    });
  };

  /*handles event when user presses arrow key to see new movie in movie-details/popup */
  handleKeyPress = event => {
    switch (event.key) {
      case "ArrowRight": {
        this.props.dispatch(
          nextMovie(this.props.selectedMovie, this.props.movies)
        );
        break;
      }
      case "ArrowLeft": {
        this.props.dispatch(
          prevMovie(this.props.selectedMovie, this.props.movies)
        );
        break;
      }
      default:
    }
  };

  /*closes the movie-details/popup */
  hideDetails = () => {
    this.props.dispatch({ type: "UNSELECT_MOVIE" });
    this.props.dispatch({ type: "RESET_REVIEW_FIELDS" });
    this.props.dispatch({ type: "SET_HOVER", payload: false });
  };

  addRating = () => {
    if (this.props.rating > 0) {
      this.props.selectedMovie["feedbackList"].push({
        name: "",
        rating: this.props.rating,
        review: ""
      });
      this.props.dispatch(updateMovie(this.props.selectedMovie));
    } else {
      console.log("error: cant rate movie zero stars");
    }
  };

  addReview = () => {
    if (this.props.openReview) {
      if (this.props.rating > 0) {
        this.props.selectedMovie["feedbackList"].push({
          name: this.props.name.trim(),
          rating: this.props.rating,
          review: this.props.review.trim()
        });
        this.props.dispatch(updateMovie(this.props.selectedMovie));
      } else {
        console.log("feltene er ikke fylt ut");
      }
    } else {
      this.props.dispatch({ type: "OPEN_REVIEW", payload: true });
    }
  };

  render() {
    let movie = this.props.selectedMovie;
    const { classes } = this.props;

    return (
      <div>
        <div className={classes.movieCard}>
          <Grid container spacing={0} style={{ margin: 0 }}>
            <Grid item xs={5}>
              <img
                src={"https://image.tmdb.org/t/p/w500" + movie.poster_path}
                alt={movie.title}
                className={classes.poster}
              />
            </Grid>
            <Grid item xs style={{ padding: "24px" }}>
              <span
                className={classes.header}
                style={{ display: "inline-block" }}
              >
                {String(movie.title).toUpperCase()}
              </span>
              <br />
              <span
                onClick={this.searchByYear}
                className={classes.link}
                style={{ cursor: "pointer" }}
              >
                {movie.year}
              </span>
              &nbsp; {"Directed by "}
              {movie.director.map((director, index) => (
                <span key={"key"}>
                  <span
                    className={classes.link}
                    style={{ cursor: "pointer" }}
                    onClick={() =>
                      this.props.dispatch({
                        type: "SHOW_MOVIE_LIST",
                        showMovies: true
                      })
                    }
                  >
                    {director.name}
                  </span>
                  {movie.director.length !== index + 1 && ", "}
                </span>
              ))}
              {averageRating(movie.feedbackList) > 0 && (
                <span>
                  <RatingComponent
                    rating={averageRating(movie.feedbackList)}
                    text1="User"
                    text2={movie.feedbackList.length}
                  />
                </span>
              )}
              <RatingComponent
                text1={"IMDb"}
                text2={movie.vote_count}
                rating={movie.vote_average}
              />
              <span className={classes.greenHeader}>{movie.tagline}</span>
              <span> {movie.overview} </span>
              <span className={classes.greenHeader}>
                {printList(movie.genres)}
              </span>
              <span>{printList(movie.production_companies)}</span>
              <span className={classes.greenHeader}> Cast </span>{" "}
              {movie.cast.slice(0, 10).map((cast, index) => (
                <span key={index}>
                  <span className={classes.link}>{cast.name}</span>
                  {index !== 9 && ", "}
                </span>
              ))}
              <br />
              <hr style={{ marginBottom: 3, marginTop: 30 }} size={1} />
              {this.props.openReview && (
                <AddReviewComponent classes={classes} />
              )}
              {!this.props.openReview && (
                <span style={{ paddingRight: "5%" }}>
                  <br />
                  Rate {movie.title}
                  <span>
                    &nbsp;
                    {"- User rating: "}
                    {averageRating(movie.feedbackList) > 0 ? (
                      <span>
                        {averageRating(movie.feedbackList)}
                        <span style={{ fontSize: "0.65em" }}>/10</span>
                        <span
                          style={{
                            fontSize: "0.7em",
                            color: "rgb(200,200,200)"
                          }}
                        >
                          (votes: {movie.feedbackList.length})
                        </span>
                      </span>
                    ) : (
                      "No users have voted"
                    )}
                  </span>
                  <br />
                  <StarRatings
                    id="rating-input1"
                    rating={this.props.rating}
                    starRatedColor="#C39400"
                    starHoverColor="#C39400"
                    changeRating={this.changeRating}
                    name="rating-input1"
                    numberOfStars={10}
                    starDimension="25px"
                    starSpacing="1px"
                  />
                  <br />
                  <br />
                  <Button onClick={this.addRating} variant="contained">
                    rate
                  </Button>
                </span>
              )}
              <span>
                <Button
                  id="reviewButton"
                  onClick={this.addReview}
                  variant="contained"
                >
                  {this.props.openReview ? "post review" : "add review"}
                </Button>
              </span>
              <br />
              <br />
              <ReviewsComponent
                feedbackList={movie.feedbackList.filter(fb => fb.name !== "")}
                averageUserScore={averageRating(
                  movie.feedbackList.filter(fb => fb.name !== "")
                )}
              />
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}

function printList(list) {
  let string = "";
  for (let i = 0; i < list.length; i++) {
    string += list[i];
    if (i !== list.length - 1) {
      string += ", ";
    }
  }
  return string;
}

function averageRating(feedbackList) {
  let total = 0;
  for (let i = 0; i < feedbackList.length; i++) {
    total += feedbackList[i].rating;
  }
  return (total / feedbackList.length).toFixed(1);
}

MovieDetailsContent.propTypes = {
  classes: PropTypes.object.isRequired
};

const styles = theme => ({
  paper: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%) !important",
    width: "85%",
    height: "auto",
    minHeight: "75%",
    backgroundColor: "black",
    boxShadow: theme.shadows[5],
    margin: "0 auto",
    outline: "none"
  },
  movieCard: {
    color: "white",
    fontFamily: "Oswald",
    fontSize: "1em",
    backgroundColor: "rgba(0,0,0,0.75)",
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%) !important",
    width: "60%",
    height: "66%",
    overflowY: "scroll",
    outline: "none"
  },
  poster: {
    width: "100%",
    position: "sticky",
    top: 0
  },
  header: {
    fontFamily: "Lato",
    fontWeight: "bold",
    fontSize: "2.2em",
    margin: 0,
    display: "inline"
  },
  greenHeader: {
    display: "block",
    marginTop: "1em",
    color: "#02FB87",
    fontSize: "1.3em",
    fontFamily: "Oswald",
    fontWeight: "normal"
  },
  textInput: {
    color: "white"
  },
  bootstrapRoot: {
    "label + &": {
      marginTop: theme.spacing.unit * 3
    }
  },
  bootstrapInput: {
    borderRadius: 4,
    backgroundColor: "rgb(200,200,200)",
    border: "1px solid #ced4da",
    fontSize: 16,
    padding: "10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"])
  },
  bootstrapFormLabel: {
    fontSize: 18,
    color: "white",
    "&:focus": {
      color: "white"
    }
  },
  margin: {
    margin: theme.spacing.unit
  },
  link: {
    color: "white",
    textDecoration: "underline"
  },

  exitForm: {
    float: "right",
    cursor: "pointer"
  }
});

const mapStateToProps = state => ({
  movies: state.search.movies,
  selectedMovie: state.selectMovie.selectedMovie,
  name: state.review.name,
  rating: state.review.rating,
  review: state.review.review,
  hover: state.closeButton.hover,
  openReview: state.review.openReview,
  searchText: state.textField.searchText,
  selectedYear: state.yearField.selectedYear,
  years: state.yearField.years,
  selectedGenre: state.genreField.selectedGenre,
  genres: state.genreField.genres,
  sortedMovies: state.sort.sortedMovies,
  page: state.pageReducer,
  fetching: state.search.fetching,
  returnedNull: state.search.returnedNull,
  sortBy: state.sort.sortBy
});

export default connect(mapStateToProps)(
  withStyles(styles)(MovieDetailsContent)
);
