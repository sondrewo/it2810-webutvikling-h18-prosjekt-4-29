import React, { Component } from "react";
import { connect } from "react-redux";
import MaterialIcon from "material-icons-react";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import Input from "@material-ui/core/Input";
import { search } from "../actions/searchAction";

const styles = {
  cssUnderline: {
    "&:hover:before": {
      backgroundColor: "white",
      height: 1
    },
    "&:before": {
      borderBottomColor: "white"
    },
    "&:after": {
      borderBottomColor: "green"
    }
  },
  searchText: {
    color: "white"
  }
};

class InputFieldComponent extends Component {
  /*handles event where user writes in input field */
  handleTextChange = event => {
    let text = event.target.value;
    let oldText = this.props.searchText;
    if (oldText === "None") {
      oldText = "";
    }
    if (text.length > 2 || text.length < oldText.length) {
      if (text.trim() === "") {
        text = "None";
      }
      this.props.dispatch(
        search(
          this.props.sortBy,
          text,
          this.props.selectedYear,
          this.props.selectedGenre
        )
      );
    }
    if (text === "") {
      text = "None";
    }
    this.props.dispatch({ type: "UPDATE_TEXT", payload: text });
  };

  render() {
    let textPlaceholder = this.props.searchText;
    if (textPlaceholder === "None") {
      textPlaceholder = "";
    }
    const { classes } = this.props;

    return (
      <div style={searchParts}>
        <Input
          onKeyPress={this.props.handleKeypress}
          id="standard-full-width"
          name="searchBox"
          value={textPlaceholder}
          style={{ margin: 0, width: "50%" }}
          placeholder="For example 'Iron Man' .."
          fullWidth
          margin="dense"
          onChange={this.handleTextChange}
          classes={{
            root: classes.searchText,
            underline: classes.cssUnderline
          }}
        />
        <div
          style={{
            cursor: "pointer"
          }}
        >
          <MaterialIcon
            onClick={this.props.search}
            icon="search"
            color="white"
          />
        </div>
        <div style={{ marginLeft: "5px", cursor: "pointer" }}>
          <MaterialIcon
            onClick={() =>
              this.props.dispatch({ type: "SHOW_INFO", payload: true })
            }
            icon="info"
            color="white"
          />
        </div>
      </div>
    );
  }
}

InputFieldComponent.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  searchText: state.textField.searchText,
  sortBy: state.sort.sortBy,
  selectedGenre: state.genreField.selectedGenre,
  selectedYear: state.yearField.selectedYear
});

export default connect(mapStateToProps)(
  withStyles(styles)(InputFieldComponent)
);

let searchParts = {
  display: "flex",
  alignItems: "center",
  alignContent: "center",
  justifyContent: "center",
  marginTop: "40px"
};
