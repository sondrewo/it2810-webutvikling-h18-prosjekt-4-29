import React, { Component } from "react";
import { connect } from "react-redux";
import Pagination from "material-ui-flat-pagination";
import { changePage } from "../actions/changePageAction";
import { withStyles } from "@material-ui/core/styles";

const styles = {
  selectedPage: {
    color: "white"
  },
  otherPages: {
    color: "#02FB87"
  },
  dots: {
    color: "#02FB87"
  },
  root: {
    color: "red"
  }
};

class PageViewerComponent extends Component {
  /*handles event where user selects a new page */
  changePage = offset => {
    let newPage = offset + 1;
    let pageCount = Math.ceil(this.props.resultsCount / 15);
    this.props.dispatch(
      changePage(
        newPage,
        this.props.page,
        pageCount,
        this.props.movies,
        this.props.sortBy,
        this.props.searchText,
        this.props.selectedYear,
        this.props.selectedGenre
      )
    );
  };

  render() {
    const { classes } = this.props;
    return (
      <div style={pages}>
        <Pagination
          limit={1}
          offset={this.props.page - 1}
          total={this.props.resultsCount / 15}
          onClick={(e, offset) => this.changePage(offset)}
          classes={{
            rootCurrent: classes.selectedPage,
            rootStandard: classes.otherPages,
            rootEnd: classes.dots
          }}
          innerButtonCount={4}
          outerButtonCount={3}
          size="small"
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  page: state.pageReducer.page,
  movies: state.search.movies,
  searchText: state.textField.text,
  selectedYear: state.yearField.selectedYear,
  selectedGenre: state.genreField.selectedGenre,
  sortBy: state.sort.sortBy,
  resultsCount: state.search.resultsCount
});

export default connect(mapStateToProps)(
  withStyles(styles)(PageViewerComponent)
);

let pages = {
  margin: "auto",
  marginTop: "40px",
  color: "white"
};
