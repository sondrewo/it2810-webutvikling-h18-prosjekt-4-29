import React, { Component } from "react";
import Modal from "@material-ui/core/Modal";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { connect } from "react-redux";

class SelectedMovieComponent extends Component {
  hideInfo = () => {
    this.props.dispatch({ type: "SHOW_INFO", payload: false });
  };

  render() {
    const { classes } = this.props;

    return (
      <Modal
        open={this.props.showInfo}
        onBackdropClick={this.hideInfo}
        onEscapeKeyDown={this.hideInfo}
      >
        <div className={classes.paper} onKeyDown={this.handleKeyPress}>
          <span style={{ fontSize: "2em" }}>Movie Search</span>
          <br />
          <br />
          <span>Search for movie titles from over 10,000 movies</span>
          <br />
          <br />
          <span>Click on a movie for more information or to add a review</span>
          <br />
          <br />
          <span>
            Search for movie by director by typing: "director " before input{" "}
          </span>
          <br />
          <span>
            {" "}
            Search for movie featuring an actor/actress by typing: "cast "
            before input{" "}
          </span>
          <br /> <br />
          Example searches: <br />
          "director Ridley Scott"
          <br /> "cast DiCaprio"
        </div>
      </Modal>
    );
  }
}

SelectedMovieComponent.propTypes = {
  classes: PropTypes.object.isRequired
};

const styles = theme => ({
  paper: {
    padding: "50px 25px",
    color: "white",
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%) !important",
    backgroundColor: "rgba(0,0,0,0.75)",
    boxShadow: theme.shadows[5],
    margin: "0 auto",
    outline: "none"
  },
  backdrop: {
    opacity: "0.8",
    filter: "blur(3px)"
  }
});

const mapStateToProps = state => ({
  showInfo: state.search.showInfo
});

export default connect(mapStateToProps)(
  withStyles(styles)(SelectedMovieComponent)
);
