const initialState = {
  selectedMovie: null,
  showMovies: false,
  hoverMovie: false,
  hoveredMovieID: ""
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case "SELECT_MOVIE": {
      return {
        ...state,
        selectedMovie: action.selectedMovie
      };
    }
    case "UNSELECT_MOVIE": {
      return {
        ...state,
        selectedMovie: null
      };
    }
    case "HOVER_MOVIE": {
      return {
        ...state,
        hoverMovie: true,
        hoveredMovieID: action.payload
      };
    }
    case "UNHOVER_MOVIE": {
      return {
        ...state,
        hoverMovie: false
      };
    }
    case "SHOW_MOVIE_LIST": {
      return {
        ...state,
        showMovies: action.showMovies
      };
    }
    case "ADD_FEEDBACK": {
      return {
        ...state,
        selectedMovie: action.updatedMovie
      };
    }
    default:
      return state;
  }
}
