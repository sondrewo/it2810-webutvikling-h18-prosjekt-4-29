const initialState = {
  name: "",
  rating: 0,
  review: "",
  openReview: false
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case "RESET_REVIEW_FIELDS": {
      return {
        ...state,
        name: "",
        rating: 0,
        review: ""
      };
    }
    case "UPDATE_NAME": {
      return {
        ...state,
        name: action.name
      };
    }
    case "UPDATE_RATING": {
      return {
        ...state,
        rating: action.rating
      };
    }
    case "UPDATE_REVIEW": {
      return {
        ...state,
        review: action.review
      };
    }
    case "OPEN_REVIEW": {
      return {
        ...state,
        openReview: action.payload
      };
    }
    default:
      return state;
  }
}
