const initialState = {
  chartData: [],
  openChart: false
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case "UPDATE_CHART": {
      return {
        ...state,
        chartData: action.payload
      };
    }
    case "OPEN_CHART": {
      return {
        ...state,
        openChart: true
      };
    }
    case "CLOSE_CHART": {
      return {
        ...state,
        openChart: false
      };
    }
    default:
      return state;
  }
}
