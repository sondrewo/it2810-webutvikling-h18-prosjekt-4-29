const initialState = {
  sortBy: "+vote_average"
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case "UPDATE_SORT": {
      return {
        ...state,
        sortBy: "+" + action.payload
      };
    }
    case "CHANGE_SORT_DIRECTION": {
      let firstLetter = state.sortBy.charAt(0);
      if (firstLetter === "+") {
        firstLetter = "-";
      } else if (firstLetter === "-") {
        firstLetter = "+";
      }

      return {
        ...state,
        sortBy: firstLetter + state.sortBy.substring(1)
      };
    }

    default:
      return state;
  }
}
