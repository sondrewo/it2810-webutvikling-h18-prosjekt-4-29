const initialState = {
  years: ["None"],
  selectedYear: "None"
};

for (let i = 2018; i > 1918; i--) {
  initialState.years.push(String(i));
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case "UPDATE_YEAR": {
      return {
        ...state,
        selectedYear: action.payload
      };
    }
    default:
      return state;
  }
}
