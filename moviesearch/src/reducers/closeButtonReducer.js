const initialState = {
  hover: false
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case "SET_HOVER": {
      return {
        ...state,
        hover: action.payload
      };
    }
    default:
      return state;
  }
}
