const initialState = {
  fetching: false,
  fetched: false,
  movies: {},
  directorMovies: [],
  returnedNull: false,
  error: null,
  resultsCount: 0,
  showInfo: false
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case "FETCH_MOVIES_START": {
      return {
        ...state,
        fetching: true
      };
    }
    case "CLEAR_MOVIES": {
      return {
        ...state,
        movies: {}
      };
    }
    case "RECIEVE_MOVIES": {
      if (action.payload === undefined) {
        return {
          ...state,
          movies: {},
          returnedNull: true,
          fetched: true,
          fetching: false
        };
      } else {
        return {
          ...state,
          returnedNull: false,
          fetching: false,
          fetched: true,
          movies: { ...state.movies, [action.page]: action.payload }
        };
      }
    }
    case "RECEIVE_DIRECTOR_MOVIES": {
      return {
        ...state,
        directorMovies: action.payload
      };
    }
    case "FAILED_FETCH": {
      return {
        ...state,
        fetching: false,
        movies: [],
        returnedNull: true,
        error: action.payload
      };
    }
    case "SET_RESULTS_COUNT": {
      return {
        ...state,
        resultsCount: action.payload
      };
    }
    case "SHOW_INFO": {
      return {
        ...state,
        showInfo: action.payload
      };
    }
    default:
      return state;
  }
}
