const initialState = {
  genres: [
    "None",
    "Horror",
    "Action",
    "Drama",
    "Thriller",
    "Romance",
    "Adventure",
    "Family",
    "Fantasy",
    "Comedy",
    "Science Fiction",
    "Animation",
    "Mystery",
    "Crime"
  ],
  selectedGenre: "None"
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case "UPDATE_GENRE": {
      console.log(action.payload);
      return {
        ...state,
        selectedGenre: action.payload
      };
    }
    default:
      return state;
  }
}
