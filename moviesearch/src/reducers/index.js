import { combineReducers } from "redux";
import search from "./searchReducer";
import sort from "./sortReducer";
import textField from "./textFieldReducer";
import yearField from "./yearReducer";
import genreField from "./genreReducer";
import review from "./reviewReducer";
import selectMovie from "./selectMovieReducer";
import cacheImages from "./selectMovieReducer";
import pageReducer from "./pageReducer";
import closeButton from "./closeButtonReducer";
import showMovies from "./selectMovieReducer";
import chart from "./ChartReducer";

export default combineReducers({
  search,
  sort,
  textField,
  selectMovie,
  cacheImages,
  yearField,
  genreField,
  pageReducer,
  closeButton,
  showMovies,
  review,
  chart
});
