const initialState = {
  page: 1
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case "SET_PAGE": {
      return {
        ...state,
        page: action.payload
      };
    }
    case "RESET": {
      return {
        ...state,
        page: 1
      };
    }
    default:
      return state;
  }
}
