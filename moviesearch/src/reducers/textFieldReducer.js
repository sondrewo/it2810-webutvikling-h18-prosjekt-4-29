const initialState = {
  searchText: "None"
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case "UPDATE_TEXT": {
      return {
        ...state,
        searchText: action.payload
      };
    }
    default:
      return state;
  }
}
