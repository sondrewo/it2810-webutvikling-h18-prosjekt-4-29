/**
 * What: Setup file for Express server, defines routes etc...
 *
 * How: Uses express to define server port, routes, 404 response etc...
 *
 */

import express from "express";
import path from "path";
import bodyParser from "body-parser";
import logger from "morgan";
import mongoose, { mongo } from "mongoose";
import SourceMapSupport from "source-map-support";
import movieRoutes from "./routes/movie.server.route";
import movieSearchRoutes from "./routes/movie.search.route";
import movieSortRoutes from "./routes/movie.sort.route";
import movieCountRoutes from "./routes/movie.count.route";
var Movie = require("./models/movie.server.model");

// define our app using express services
const app = express();

// allow-cors
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// configure app
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "public")));

// set the port of the application back-end
const port = process.env.PORT || 3001;

// connect to database (Passord i klartekst FTW)
mongoose.Promise = global.Promise;
mongoose.connect(
  "mongodb://ihsadmin:heieh@it2810-29.idi.ntnu.no:27017/p4",
  {
    useNewUrlParser: true
  }
);
// add Source Map Support for logging
SourceMapSupport.install();

//ROUTING FOR CRUD OPERATIONS
app.use("/api", movieRoutes);
app.use("/search", movieSearchRoutes);
app.use("/sort", movieSortRoutes);
app.use("/count", movieCountRoutes);

// catch 404 error
app.use((req, res, next) => {
  res
    .status(404)
    .send("<h2 align=center margin=500px>Oh No! Something went wrong!!</h2>");
});

// start the server
app.listen(port, () => {
  console.log(`App Server Listening at ${port}`);
});
